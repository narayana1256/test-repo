public class JSScript6 {
  public List<contact> con{get;set;}
     
     public JSScript6 (){
     con= [select name,Mailingcity,Mailingstate from contact];
     }

     @RemoteAction
     public static Contact deleteContact(Id conid){
     Contact c = new Contact();
     c.Id=conid;
     delete c;
     return c;
     }

     @RemoteAction
     public static Contact editContact(Id conid){
     Contact c = new Contact();
     c=[select firstname,lastname,phone,mailingstreet,otherstreet,mailingcity,mailingstate,fax from contact where id=:conid limit 1]; 
     return c;
     }

     @RemoteAction
     public static Contact saveContact(String fName, String lName, String addr1, String addr2, String phno, String fax, String city, String state, String conId){
     Contact c = new Contact();
     c.LastName = lName;
     c.FirstName = fName;
     c.MailingStreet = addr1;
     c.OtherStreet = addr2;
     c.Phone = phno;
     c.Fax = fax;
     c.MailingState = state;
     c.MailingCity = city;
     if(conId!=''){
     c.Id = conId;
     update c;
     } else insert c;
     return c;
     }
     
     
     @RemoteAction
     public static List<Contact> searchContacts(String name){
     system.debug(name);
     String sql = 'select Name,Mailingcity,Mailingstate from contact where Name Like \'%' + name + '%\' ';
     system.debug(sql);
     List<Contact> con = database.query(sql);
     system.debug(con);
     return con;
     }

}